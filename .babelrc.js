module.exports = {
    "presets": [
        [
            "@babel/preset-env",
            {
                "modules": false
            }
        ],
        "@babel/preset-react",
        "@babel/typescript"
    ],
    "plugins": [
        "@babel/proposal-class-properties",
        "@babel/proposal-object-rest-spread",
        ["@babel/plugin-proposal-decorators", { "legacy": true }],
        "@babel/plugin-proposal-export-default-from",
        "@babel/plugin-syntax-dynamic-import",
        [
            "import",
            {
                "libraryName": "antd",
                "style": true
            },
            'antd'
        ],
        [
            "import",
            {
                "libraryName": "component-library-template",
                "libraryDirectory": "lib",
                "style": (name) => {
                    console.log(name, '动态引入，样式不起作用');
                    return `${name}/style/index.css`;
                }
            },
            "component-library-template"
        ],
        "lodash"
    ],
    "comments": false,
    "ignore": ["dist/*.js"],
    "env": {
        "test": {
            "presets": ["@babel/preset-env", "@babel/preset-react", "@babel/typescript"],
            "plugins": [
                "@babel/proposal-class-properties",
                "@babel/proposal-object-rest-spread",
                ["@babel/plugin-proposal-decorators", { "legacy": true }],
                "@babel/plugin-proposal-export-default-from",
                "@babel/plugin-syntax-dynamic-import",
                [
                    "import",
                    {
                        "libraryName": "antd",
                        "style": true
                    }
                ],
                "lodash"
            ]
        }
    }
}
