import React, { useState, useCallback } from 'react';

function App() {
  const [data, setData] = useState('');
  const onClick = useCallback(() => {
    setData('xiaoma');
  }, []);
  return (
    <div>
      <button type="button" onClick={onClick}>
        改变
      </button>
      <div>{data}</div>
    </div>
  );
}
export default App;
