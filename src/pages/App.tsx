import * as React from 'react';
import { BrowserRouter as Router, Switch, Route } from 'react-router-dom';
import WithLoadable from '@components/WithLoadable';

const Home = WithLoadable(() => import('./Test'));
const Login = WithLoadable(() => import('./Login'));
const Editor = WithLoadable(() => import('./richeditor'));
const Jsx = WithLoadable(() => import('./jsx'));

const App = () => (
  <Router>
    <Switch>
      <Route path="/login" component={Login} />
      <Route path="/editor" component={Editor} />
      <Route path="/jsx" component={Jsx} />
      {/* 这样就可以直接访问子路由了 */}
      <Home />
    </Switch>
  </Router>
);
export default App;
