import React from 'react';

interface TabProps{
    id: string;
    title: string;
    active?: string;
    toggleActive?: (e: React.MouseEvent,id?:string) => void;
}
export function Tab(props:TabProps) {
  const { id, title, active, toggleActive } = props;
  return (
    <a onClick={(e) => toggleActive && toggleActive(e)}>
    { title }
    { active === id ? <span>id</span> : null }
    </a>
  )
}
export interface TabGroupProps {
  active: string;
}
const TabGroup:React.FC<TabGroupProps>= ({active,children}):any=> {
  const toggleActive = (e: any, id: string) => {
    e.preventDefault();
    // setActive(id);
  };
  return React.Children.map(children, (child:any) => {
    const enhancedChild = React.cloneElement(child, {
      active,
      toggleActive: toggleActive
    });
    return enhancedChild;
  });
}

const TabGroupContainer = () => {
  return <TabGroup active="a">
      <Tab id="a" title="a"></Tab>
      <Tab id="b" title="b"></Tab>
      <Tab id="c" title="c"></Tab>
    </TabGroup>
}
export default TabGroupContainer;
