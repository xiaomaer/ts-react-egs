import * as React from 'react';
import RichEditor from '@beisen-phoenix/richeditor';

export default function Test() {
  /* const options = {
    id: 'richeditor',
    value: '<p>xiaoma</p>',
    name: '测试',
    videoConfig: {
      //视频相关配置
      autoplay: false, //自动播放，默认false
      loop: true //循环播放，默认false
    },
    required: true,
    errorStatus: false, //是否报错
    errorMsg: '出错了~~~！', //报错信息
    helpMsg: 'dqwdqw',
    sideTip: false, //toolTip是否左右显示
    hiddenTip: false, //toolTip是否显示
    editStatus: true,
    lablePos: false, //label位置，true时在左边，false在上边
    lableTxt: false, //label中文字对齐方式，true左对齐，false右对齐
    imageUrl: 'http://demo-cloud.italent.link/api/v2/Data/File', //本地上传时的上传地址，默认为当前页面地址
    parameter: 'app=BeisenCloudDemo&metaObjName=BeisenCloudDemo.123', //本地上传时的参数，开头不加 &
    localUpload: true, //开启本地上传功能
    fileType: ['.jpeg', '.jpg', '.gif', '.png', '.bmp'],
    fileSize: '10M',
    fileSizeMessage: '上传图片不允许超过10M',
    fileTypeMessage: '上传图片格式错误',
    toolbar: [
      'source | bold italic underline strikethrough superscript subscript | forecolor backcolor | fullscreen |',
      'insertorderedlist insertunorderedlist | cleardoc selectall removeformat print | paragraph fontfamily fontsize',
      '| justifyleft justifycenter justifyright indent outdent |',
      'link unlink emotion formula | image video music',
      '| horizontal preview inserttable drafts'
    ],
    onChange: function(value: string, haveContent: boolean) {
      console.log(value);
      console.log(haveContent);
    },
    onUploadSuccess: (event: any, xhr: any, response: any) => {
      console.log('上传成功');
      console.log('====', event, xhr, response);
    },
    onUploadFailure: (event: any, xhr: any, response: any) => {
      console.log('上传失败');
      console.log('====', event, xhr, response);
    },
    lang: 'es',
    pasteFormat: (html: string) => {
      return html.replace(/font-family: PingFangSC-Regular;/g, '');
    }
  }; */
  return (
    <div style={{ padding: 20 }}>
      <RichEditor />
    </div>
  );
}
