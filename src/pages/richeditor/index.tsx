import React, { Component } from 'react';
import { init, exec } from 'pell';
import 'pell/dist/pell.css';

interface Props {}
interface State {
  html: string | null;
}

export default class RichEditor extends Component<Props, State> {
  editor = null;

  state = {
    html: null
  };

  componentDidMount() {
    const actions = [
      'bold',
      'italic',
      'underline',
      'strikethrough',
      'heading1',
      'heading2',
      'paragraph',
      'quote',
      'olist',
      'ulist',
      'code',
      'line',
      'link',
      'image',
      {
        name: 'backColor',
        icon: '<div style="background-color:pink;">A</div>',
        title: 'Highlight Color',
        result: () => exec('backColor', 'pink')
      }
      /* 'justifyCenter',
      'justifyFull',
      'justifyLeft',
      'justifyRight',
      'subscript',
      'superscript',
      'fontName',
      'fontSize',
      'indent',
      'outdent',
      'redo',
      'undo',
      'removeFormat' */
    ];
    this.editor = init({
      element: document.getElementById('editor'),
      onChange: (html: string) => this.setState({ html }),
      actions
    });
  }

  render() {
    return (
      <div className="App">
        <h3>Editor:</h3>
        <div id="editor" className="pell" />
        <h3>HTML Output:</h3>
        <div id="html-output">{this.state.html}</div>
      </div>
    );
  }
}
